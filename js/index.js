'use strict';
(function (app) {

var components = app.components;

function start() {
    var options = {
        el: '#app',
        data: {
            carouselOptions: {
                adaptiveHeight: true,
                arrows: false,
                dots: true,
                fade: false,
                mobileFirst: true
            }
        },
        mixins: [],
        computed: {
            carousel: function () {
                return this.$refs.carousel;
            }
        },
        components: {
            'air-quality-average': components.airQualityAverage,
            'air-quality-single': components.airQualitySingle,
            pm25: components.pm25,
            pm10: components.pm10,
            ozone: components.ozone,
            freeze: components.freeze,
            tabbar: components.tabbar
        },
        mounted: function () {
            var context = this;
            context.$nextTick(function () {
                $(context.carousel).slick(context.carouselOptions);
            });

            $(window).on('keyup', function () {
                if(event.keyCode !== 32) { // 空格键
                    return;
                }
                var currentSlide = $(context.carousel).slick('slickCurrentSlide');
                context.$emit('toggle-popup', currentSlide);
            });
        }
    };
    app.tabbar && options.mixins.push(app.tabbar);
    app.vm = new Vue(options);
}

start();

})(app);