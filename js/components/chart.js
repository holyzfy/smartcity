'use strict';
(function (app) {

var components = app.components;
var common = app.common;
var urlMap = common.urlMap;

components.chart = {
    name: 'chart',
    template: '#template-chart',
    props: ['defaultDataType', 'defaultChartType'],
    data: function () {
        return {
            dataType: this.defaultDataType || 'pm25',
            chartType: this.defaultChartType || 'Bar',
            chartDateTime: '',
            chartData: null
        };
    },
    mixins: [],
    watch: {
        dataType: function () {
            this.render();
        }
    },
    mounted: function () {
        var context = this;
        getData(function (result) {
            context.chartData = result;
            context.render();
        });
    },
    methods: {
        render: function () {
            render(this);
        }
    }
};

function getData(callback) {
    $.getJSON(urlMap.chart, function (result) {
        if(result.status !== 0) {
            return;
        }

        var ret = {};
        for(var key in result.data) {
            var value = result.data[key];
            var labels = [];
            var series = [];
            /* eslint-disable no-loop-func */
            value.forEach(function (item) {
                var pair = item.split(':');
                labels.push(pair[0]);
                series.push(+pair[1]);
            });
            ret[key] = {
                labels: labels,
                series: series
            };
        }
        callback && callback(ret);
    });    
}

function render(context) {
    var data = context.chartData;
    var chartData = {
        labels: data[context.dataType].labels,
        series: data[context.dataType].series
    };
    var options = {
        distributeSeries: true,
        axisX: {
            offset: 15
        },
        axisY: {
            offset: 33
        },
        high: 300, // AQI大于300，空气质量六级
        chartPadding: {
            left: 0,
            right: 0,
            bottom: 3,
            top: 5
        }
    };
    var responsiveOptions = [
        ['(max-width: 768px)', {
            axisX: {
                labelInterpolationFnc: function skipLabels(value, index, labels) {
                    return index % 2 === 0 ? +value : null;
                }
            }
        }]
    ];
    var chart = new Chartist[context.chartType](context.$refs.chart, chartData, options, responsiveOptions);
    chart.on('draw', function(context) {
        if(context.type === 'bar') {
            var level = common.getAqiLevel(context.series);
            context.group.addClass('ct-level-' + level);
            context.element.animate({
                y2: {
                    dur: '0.5s',
                    from: context.y1,
                    to: context.y2
                }
            });
        }
    });
}

})(app);