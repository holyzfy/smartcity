'use strict';
(function (app) {

var components = app.components;

var options = {
    name: 'pm25',
    template: '#template-pm25',
    mixins: [],
    mounted: function () {
        this.popup = 'chart';
        initMap(this.$refs.map);
    }
};

var popupOptions = {
    currentSlide: 2
};
app.getPopup && options.mixins.push(app.getPopup(popupOptions));
components.pm25 = options;

function initMap(el) {
    var map = new BMap.Map(el);
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
    map.setCurrentCity('北京');
    map.enableScrollWheelZoom(true);
}

})(app);