'use strict';
(function (app) {

var components = app.components;
var common = app.common;
var urlMap = common.urlMap;

var options = {
    name: 'air-quality-single',
    template: '#template-air-quality-single',
    mixins: [common.aqi],
    components: {
        dashboard: components.dashboard
    },
    created: function () {
        var context = this;
        $.getJSON(urlMap['aqi-single'], function (result) {
            if(result.status !== 0) {
                return;
            }
            context.aqi = result.data;
        });
    }
};

components.qrcode && options.mixins.push(components.qrcode);

components.airQualitySingle = options;

})(app);