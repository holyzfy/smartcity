'use strict';
(function (app) {

var components = app.components;

components.tabbar = {
    name: 'tabbar',
    template: '#template-tabbar',
    data: function () {
        return {
            autoplay: this.$root.carouselOptions.autoplay
        };
    },
    watch: {
        autoplay: function (boolean) {
            this.updateCarouselOptions('autoplay', boolean);
        }
    },
    methods: {
        updateCarouselOptions: function(key, value) {
            this.$emit('carousel-update', key, value);
        }
    }
};

})(app);