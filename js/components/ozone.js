'use strict';
(function (app) {

var components = app.components;

var options = {
    name: 'ozone',
    template: '#template-ozone',
    mixins: [],
    mounted: function () {
        this.popup = 'chart';
        initMap(this.$refs.map);
    }
};

var popupOptions = {
    currentSlide: 4
};
app.getPopup && options.mixins.push(app.getPopup(popupOptions));
components.ozone = options;

function initMap(el) {
    var map = new BMap.Map(el);
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
    map.setCurrentCity('北京');
    map.enableScrollWheelZoom(true);
}

})(app);