'use strict';
(function (app) {

var components = app.components;
var common = app.common;
var urlMap = common.urlMap;

var options = {
    name: 'air-quality-average',
    template: '#template-air-quality-average',
    mixins: [common.aqi],
    components: {
        dashboard: components.dashboard,
        chart: components.chart
    },
    created: function () {
        var context = this;
        $.getJSON(urlMap['aqi-average'], function (result) {
            if(result.status !== 0) {
                return;
            }
            context.aqi = result.data;
        });
    }
};

components.qrcode && options.mixins.push(components.qrcode);

components.airQualityAverage = options;

})(app);