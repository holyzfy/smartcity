'use strict';
(function (app) {

var components = app.components;

var options = {
    name: 'timeline',
    template: '#template-timeline',
    data: function () {
        return {
            type: 'hour', // hour, day, week
            current: 1,
            playing: false
        };
    },
    computed: {
        typeText: function () {
            var map = {
                hour: '小时',
                day: '天',
                week: '周'
            };
            return map[this.type];
        }
    },
    mixins: []
};

components.timeline = options;

})(app);