'use strict';
(function (app) {

var components = app.components;

components.qrcode = {
    name: 'qrcode',
    template: '#template-qrcode',
    methods: {
        showQrcode: showQrcode
    }
};

function showQrcode(event) {
    var options = {
        el: document.createElement('div'),
        template: '#template-qrcode',
        data: function () {
            return {
                src: event.target.src
            };
        }
    };
    var popup = new Vue(options);
    $('body').append(popup.$el);
}

})(app);