'use strict';
(function (app) {

var components = app.components;

var options = {
    name: 'freeze',
    template: '#template-freeze',
    mixins: [],
    mounted: function () {
        initMap(this.$refs.map);
    }
};

var popupOptions = {
    currentSlide: 5
};
app.getPopup && options.mixins.push(app.getPopup(popupOptions));
components.freeze = options;

function initMap(el) {
    var map = new BMap.Map(el);
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
    map.setCurrentCity('北京');
    map.enableScrollWheelZoom(true);
}

})(app);