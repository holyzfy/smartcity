'use strict';
(function (app) {

var components = app.components;
var common = app.common;

components.dashboard = {
    name: 'dashboard',
    template: '#template-dashboard',
    props: ['aqi'],
    components: {
        odometer: components.odometer
    },
    methods: {
        isHighest: isHighest,
        getLevel: common.getAqiLevel,
        getLevelText: getLevelText,
        getUnit: getUnit
    }
};

function isHighest(value, list) {
    var max = list.map(function (item) {
        return item.value;
    }).sort(function (a, b) {
        return a - b;
    }).reverse()[0];
    return value === max;
}

function getLevelText(value) {
    var level = common.getAqiLevel(value);
    var list = [
        '优',
        '良',
        '轻度污染',
        '中度污染',
        '重度污染',
        '严重污染'
    ];
    return list[level - 1];
}

function getUnit(type) {
    var map = {
        pm25: 'μg',
        so2: 'μg',
        no2: 'μg',
        ozone: 'μg',
        pm10: 'μg',
        co: 'mg'
    };
    return map[type];
}

})(app);