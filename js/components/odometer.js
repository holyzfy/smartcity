'use strict';
(function (app) {

var components = app.components;

var options = {
    name: 'odometer',
    template: '<div class="odometer"></div>',
    props: ['value'],
    data: function () {
        return {
            instance: null
        };
    },
    watch: {
        value: function (value) {
            this.instance.update(value);
        }
    },
    mounted: function () {
        var context = this;
        context.instance = new Odometer({
            el: context.$el,
            theme: 'minimal',
            value: context.value
        });
    }
};
components.odometer = options;

})(app);