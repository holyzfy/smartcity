'use strict';
(function (app) {

function getEnv() {
    var env = {
        '127.0.0.1': 'development',
        'localhost': 'development',
        'smartcity.localhost.com': 'development',
        'smartcity.f2e.yiifcms.com': 'development'
    };
    return env[location.hostname] || 'production';
}

var urlMap = {};

urlMap.development = {
    'chart': 'mock/chart.json',
    'aqi-average': 'mock/aqi-average.json',
    'aqi-single': 'mock/aqi-single.json'
};

// 生产环境的接口地址
var urlRoot = '';
urlMap.production = {
    'chart': urlRoot + '/TODO'
};

function getAqiLevel(value) {
    // http://www.bjmemc.com.cn/g374/s1046/t1662.aspx
    if(value <= 50) {
        return 1;
    }
    if(value <= 100) {
        return 2;
    }
    if(value <= 150) {
        return 3;
    }
    if(value <= 200) {
        return 4;
    }
    if(value <= 300) {
        return 5;
    }
    if(value > 300) {
        return 6;
    }
}

if(getEnv() === 'production') {
    Vue.config.silent = true;
    Vue.config.productionTip = false;
    Vue.config.errorHandler = $.noop;
}

var aqi = {
    data: function () {
        return {
            aqi: null
        };
    }
};

$.extend(app.common, {
    getEnv: getEnv,
    getAqiLevel: getAqiLevel,
    aqi: aqi,
    urlMap: urlMap[getEnv()]
});

})(app);
