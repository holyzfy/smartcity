'use strict';
(function (app) {

var components = app.components;

var tabbar = {
    data: function () {
        return {
            carouselOptions: {
                autoplay: false,
                draggable: false,
                autoplaySpeed: 15000 // ms
            },
            tabbarStatus: 'hidden'
        };
    },
    watch: {
        carouselOptions: {
            handler: function () {
                updateCarousel(this);
            },
            deep: true
        }
    },
    mounted: function () {
        $(this.carousel).on('afterChange', function (event, slick, currentSlide) {
            $('html, body').animate({
                scrollTop: 0
            });
        });
    },
    methods: {
        updateCarouselOptions: function (key, value) {
            this.carouselOptions[key] = value;
        },
        carouselGoTo: function (index) {
            $(this.carousel).slick('slickGoTo', index);
        }
    }
};

function updateCarousel(context) {
    var $carousel = $(context.carousel);
    var index = $carousel.slick('slickCurrentSlide');
    $carousel.slick('unslick').slick(context.carouselOptions).slick('slickGoTo', index, true);
}

function getPopup(options) {
    return (function (options) {
        return {
            data: function () {
                return {
                    popupVisible: false
                };
            },
            components: {
                timeline: components.timeline,
                chart: components.chart
            },
            mounted: function () {
                var context = this;
                context.$root.$on('toggle-popup', function (index) {
                    (index === options.currentSlide) && context.popupToggle();
                });
            },
            methods: {
                popupToggle: function () {
                    this.popupVisible = !this.popupVisible;
                }
            }
        };
    })(options);
}

$.extend(app, {
    tabbar: tabbar,
    getPopup: getPopup
});

})(app);